import { useEffect, useState } from 'react'
import AjoutFavorie from './components/AjoutFavorie'
import ListFavories from './components/ListFavories'
import InfoVille from './components/InfoVille'
import axios from 'axios'

function App() {
	const [ville, setVille] = useState('')
	const [villes, setVilles] = useState([])
	const [currentVille, setCurrentVille] = useState()
	let tempData = []
	let key = '0368bb4fc15009b50c69470bdce48c98'
	let baseUrl = 'https://api.openweathermap.org/data/2.5/weather?units=metric&appid='+key

	const handleChangeVille = (e)=>{
		setVille(e.target.value)
	}

	const handleDelete = (index) =>{
		const copy = [...villes]
		copy.splice(index, 1)
		setVilles(copy)
		localStorage.setItem('favoris', JSON.stringify(copy))
	}

	const handleSearch = (val) =>{
		axios.get(`${baseUrl}&q=${val}`)
		.then(({data})=>{
			setCurrentVille(data)
		})
		.catch(error=>{
			alert("Ville introuvable")
		})
	}

	useEffect(()=>{
		const favoris = localStorage.getItem('favoris')
		if (favoris) {
			let favo = JSON.parse(favoris)
			setVilles(favo)
			handleSearch(favo[0])
		}
	}, [])

	const handleSubmit = (e) =>{
		e.preventDefault()

		if (ville.trim() !== '') {
			tempData.push(...villes, ville)
			setVilles(tempData)
			localStorage.setItem('favoris', JSON.stringify(tempData))
			setVille('')
		}
		
	}

	return (
		<div className='container mt-4'>
			<div className="row mb-2">
				<AjoutFavorie handleChangeVille={handleChangeVille} handleSubmit={handleSubmit} ville={ville} />
			</div>
			<div className="row">
				<div className="col-sm-4">
					<ListFavories handleSearch={handleSearch} villes={villes} handleDelete={handleDelete} />
				</div>
				<div className="col-sm-6">
					<InfoVille ville={currentVille} />
				</div>
			</div>
		</div>
	)
}

export default App
