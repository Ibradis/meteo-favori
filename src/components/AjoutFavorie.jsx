import React from 'react';

const AjoutFavorie = ({ville, handleChangeVille, handleSubmit}) => {
    return (
        <>
            <div className="col-sm-12">
                <form onSubmit={handleSubmit} action="">
                    <div className="row">
                        <div className="col-sm-6">
                            <input onChange={handleChangeVille} value={ville} type="text" className='form-control' placeholder='Ville' />
                        </div>
                        <div className="col-sm-6">
                            <button className='btn btn-primary'>Ajouter une ville en favori</button>
                        </div>
                    </div>
                </form>
            </div>

            {/* <div className="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">Favori</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            ...
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Fermer</button>
                            <button type="button" className="btn btn-primary">Enregistrer</button>
                        </div>
                    </div>
                </div>
            </div> */}
        </>
    );
}

export default AjoutFavorie;
