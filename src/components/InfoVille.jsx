import React from 'react';

const InfoVille = ({ville}) => {
    let baseUrlImg = 'https://openweathermap.org/img/wn/'
    return (
        <div className='card'>
            <div className="card-body">
                <h3 className='text-center'>{ville && ville.name}</h3>

                <div className="card bg-primary text-light">
                    <div className="card-body">
                        <div className="row justify-content-between">
                            <div className="col-sm-6 d-flex align-item-center">
                                <span className='fw-bold display-4'>{ville && parseInt(ville.main.temp)}°C</span>
                                <span><img src={`${baseUrlImg}/${ville && ville.weather[0].icon}.png`} alt="" /></span>
                            </div>
                            <div className="col-sm-4">
                                <h6>
                                    <span>Vent : </span> 
                                    <span>{ville && parseInt(ville.wind.speed)} </span>
                                    <span>km/h</span>
                                </h6>
                                <h6>
                                    <span>Humidité : </span>
                                    <span>{ville && ville.main.humidity}%</span>
                                    {/* <span><img src={`${baseUrlImg}/13d.png`} alt="" /></span> */}
                                </h6>
                                <h6>
                                    <span>Pression : </span>
                                    <span>{ville && ville.main.pressure}</span>
                                    {/* <span><img src={`${baseUrlImg}/13d.png`} alt="" /></span> */}
                                </h6>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    );
}

export default InfoVille;
