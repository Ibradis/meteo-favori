import React from 'react';

const ListFavories = ({villes, handleDelete, handleSearch}) => {
    return (
        <div className='card'>
            <div className="card-body">
                <h4 className='text-center'>Nos Favoris</h4>
                {villes.length ?
                    <table className='table'>
                        <tbody>
                        {
                            villes.map((item, index)=>(
                                <tr key={index}>
                                    <td>
                                        <span 
                                            onClick={()=>handleSearch(item)} 
                                            className='badge bg-dark text-light p-3' 
                                            style={{width: '80%', cursor:'pointer'}}>
                                            {item}
                                        </span></td>
                                    <td align='right'><button onClick={()=>handleDelete(index)} className='btn btn-danger btn-sm'>Supprimer</button></td>
                                </tr>
                            ))
                        }
                        </tbody>
                    </table>
                    : <span className='text-center mt-2 w-100'>Liste vide</span>
                }
            </div>
        </div>
    );
}

export default ListFavories;
